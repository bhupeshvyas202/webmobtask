import React from "react";
import "./Header.css";
function Header() {
  return (
    <div className="header">
      <div className="header__left">
        <img src="https://cdn-images-1.medium.com/max/650/1*lPueDCFgjPU2o2Il8dprig@2x.png" />
        <h4>WEBMOB Technologies Task</h4>
      </div>
      <div className="header__right">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Vector_search_icon.svg/1200px-Vector_search_icon.svg.png" />
        <input placeholder="Search" />
      </div>
    </div>
  );
}

export default Header;
