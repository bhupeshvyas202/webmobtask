import React from "react";
import "./People.css";

function People({ email, firstname, lastname, url, date }) {
  return (
    <div className="people">
      <img src={url} />
      <p>Name - {`${firstname}${" "}${lastname}`}</p>
      <p>Email - {email}</p>
      <p>Date of birth - {date}</p>
    </div>
  );
}

export default People;
