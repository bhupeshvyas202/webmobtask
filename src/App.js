import React, { useEffect, useState } from "react";
import "./App.css";
import People from "./People";
import Header from "./Header";
function App() {
  const [people, setPeople] = useState([]);
  useEffect(() => {
    async function fetchPeople() {
      const res = await fetch("https://randomuser.me/api/?page=1&results=30");
      const data = await res.json();
      setPeople(data.results);
    }
    fetchPeople();
  }, []);
  console.log(people);
  return (
    <div className="app">
      <Header />
      <div className="app_people">
        {people.map((people) => {
          return (
            <People
              url={people.picture.thumbnail}
              email={people.email}
              firstname={people.name.first}
              lastname={people.name.last}
              date={people.dob.date}
            />
          );
        })}
      </div>
    </div>
  );
}

export default App;
